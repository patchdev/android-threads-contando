package com.example.patch.randroidtask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

class TipoNoPrimitivo {

    //Variables de conteo
    public int MinCount;
    public int i1Value;
    public int i2Value;
    public int i3Value;
    public int MaxCount;
    public int Step;
    public int StepsPerThread;

    //Control por flags
    public boolean turnofirstThread = true;
    public boolean turnosecondThread = false;
    public boolean turnothirdThread = false;
    public boolean turnofourthThread = false;
    public boolean turnofifthThread = false;

    public boolean finPrimeraCuenta = false;
    public boolean finSegundaCuenta  = false;
    public boolean finTerceraCuenta = false;
    public boolean finCuartaCuenta  = false;
    public boolean finQuintaCuenta = false;

    //Otras variables
    public int ordenThreadAux = 0;
    public int contadorComun = 0;
    public int multiplicadorTurnoAux = 0;


    public TipoNoPrimitivo(int MinCount, int MaxCount,int Step,int StepsPerThread,int i1Value,int i2Value,int i3Value) {
        this.MinCount = MinCount;
        this.MaxCount = MaxCount;
        this.Step = Step;
        this.StepsPerThread = StepsPerThread;
        this.i1Value = i1Value;     
        this.i2Value = i2Value;
        this.i3Value = i3Value;

        ordenThreadAux = 0;
        contadorComun = MinCount;
    }
}


class myThread extends Thread {

    public final TipoNoPrimitivo vc;

    public myThread(String threadName, TipoNoPrimitivo SharedVar) {
        vc = SharedVar;
        setName(threadName);
    }

    //*************************************************************************************
    //Realiza la cuenta, el print por pasos y al final actualiza el valor de contador común
    //**************************************************************************************
    private void Contar(int valorInicial, int finalDeCuenta){

        int pasosAux = 0;
        int multiplicadorTurno = vc.multiplicadorTurnoAux;
        int contarDesde = valorInicial + multiplicadorTurno*vc.StepsPerThread*vc.Step;
        int contarHasta = valorInicial + (multiplicadorTurno + 1)*vc.StepsPerThread*vc.Step;

        for(int contador = contarDesde; contador <= contarHasta; contador++ ){
            pasosAux++;

            //Actualizar contador compartido
            vc.contadorComun = contador;

            //Imprimir cada Pasos
            if(pasosAux >= vc.Step || contador == contarDesde || contador == contarHasta){
                pasosAux = 0;
                Log.d("myTAG", "Yo soy el " + getName() + " y mi valor del contador es: " + contador);
            }

            //Comprobar si se ha llegado al final de la cuenta del bloque, no de esta llamada
            if(vc.contadorComun == finalDeCuenta){
                vc.multiplicadorTurnoAux = -1;
                break;
            }

        }
        vc.multiplicadorTurnoAux++;
    }

    //*************************************************************************************
    //Realiza la cuenta, pero hacia atrás. Revisar
    //**************************************************************************************
    private void ContarAtras(int valorInicial, int finalDeCuenta){
        int pasosAux = 0;
        int multiplicadorTurno = vc.multiplicadorTurnoAux;
        int contarDesde = valorInicial - multiplicadorTurno*vc.StepsPerThread*vc.Step;
        int contarHasta = valorInicial - (multiplicadorTurno + 1)*vc.StepsPerThread*vc.Step;

        for(int contador = contarDesde; contador >= contarHasta; contador--){
            pasosAux++;
            //Actualizar contador compartido
            vc.contadorComun = contador;

            //Imprimir cada pasos
            if(pasosAux >= vc.Step || contador == contarDesde || contador == contarHasta){
                pasosAux = 0;
                Log.d("myTAG", "Yo soy el " + getName() + " y mi valor del contador es: " + contador);
            }

            //Comprobar si se llega al final de la cuenta total
            if(vc.contadorComun == finalDeCuenta){
                vc.multiplicadorTurnoAux = -1;
                break;
            }
        }

        vc.multiplicadorTurnoAux++;
    }

    //******************************
    //Metodos para codigos repetidos
    //******************************
    private void prepararSegundaCuenta(){
        Log.d("myTAG", "*********************************************");
        vc.finPrimeraCuenta = true;
        vc.turnofirstThread = false;
        vc.turnosecondThread = false;
        vc.turnothirdThread = false;
        vc.turnofourthThread = false;
        vc.turnofifthThread = true;
    }

    private void prepararTerceraCuenta(){
        Log.d("myTAG", "*********************************************");
        vc.finSegundaCuenta = true;
        vc.turnofirstThread = true;
        vc.turnosecondThread = false;
        vc.turnothirdThread = false;
        vc.turnofourthThread = false;
        vc.turnofifthThread = false;
    }

    private void prepararCuartaCuenta(){
        Log.d("myTAG", "*********************************************");
        vc.finTerceraCuenta = true;
        vc.turnofirstThread = false;
        vc.turnosecondThread = false;
        vc.turnothirdThread = false;
        vc.turnofourthThread = false;
        vc.turnofifthThread = true;
    }

    private void prepararUltimaCuenta(){
        Log.d("myTAG", "*********************************************");
        vc.finCuartaCuenta = true;
        vc.turnofirstThread = true;
        vc.turnosecondThread = false;
        vc.turnothirdThread = false;
        vc.turnofourthThread = false;
        vc.turnofifthThread = false;
    }

    private void espaciarThreads(){
        Log.d("myTAG", "                ");
        Log.d("myTAG", "                ");
    }

    @Override
    public void run() {

        //****************************
        //Bloque que asigna posiciones
        //****************************
        int ordenThread = 0;

        try{
            synchronized(vc)
            {
                vc.ordenThreadAux++;
                ordenThread = vc.ordenThreadAux;

                switch(ordenThread)
                {
                    case 1:
                        Log.d("myTAG", "Soy el "+ getName()+ " con ID "+ getId() + ": he llegado el primero :-P al 'run'");
                        vc.wait();
                        break;

                    case 2:
                        Log.d("myTAG", "Soy el "+ getName()+ " con ID "+ getId() + ": he llegado el segundo :-) al 'run'");
                        vc.wait();
                        break;

                    case 3:
                        Log.d("myTAG", "Soy el "+ getName()+ " con ID "+ getId() + ": he llegado el tercero :-( al 'run'");
                        vc.wait();
                        break;

                    case 4:
                        Log.d("myTAG", "Soy el "+ getName()+ " con ID "+ getId() + ": he llegado el cuarto :-S al 'run'");
                        vc.wait();
                        break;

                    case 5:
                        Log.d("myTAG", "Soy el "+ getName()+ " con ID "+ getId() + ": he llegado el ultimo @_@ al 'run'");
                        vc.notifyAll();
                        break;

                    default:
                        break;
                }
            }
        }catch(Exception e){}


        //**************************************************************************
        //Bloque donde cuentan firstThread, secondThread, thirdThread y fourthThread.
        //***************************************************************************
        while(!vc.finPrimeraCuenta){
            try{synchronized(vc){
                if((ordenThread == 1)&&(vc.turnofirstThread)){
                    //Contar
                    Contar(vc.MinCount, vc.i1Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i1Value){
                        prepararSegundaCuenta();
                        vc.notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnofirstThread = false;
                        vc.turnosecondThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 2)&&(vc.turnosecondThread)){
                    //Contar
                    Contar(vc.MinCount, vc.i1Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i1Value){
                        prepararSegundaCuenta();
                        vc.notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnosecondThread = false;
                        vc.turnothirdThread = true;
                        espaciarThreads();
                    }

                }

                if((ordenThread == 3)&&(vc.turnothirdThread)){
                    //Contar
                    Contar(vc.MinCount, vc.i1Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i1Value){
                        prepararSegundaCuenta();
                        vc.notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnothirdThread = false;
                        vc.turnofourthThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 4)&&(vc.turnofourthThread)){
                    //Contar
                    Contar(vc.MinCount, vc.i1Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i1Value){
                        prepararSegundaCuenta();
                        vc.notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnofourthThread = false;
                        vc.turnofirstThread = true;
                        espaciarThreads();
                    }
                }

                if(!vc.finPrimeraCuenta){
                    vc.notify();
                    vc.wait();
                }
            }}catch(Exception e){}
        }


        //************************************************************
        //Bloque donde cuentan fifthThread, fourthThread, thirdThread.
        //************************************************************
        while(!vc.finSegundaCuenta){
            try{synchronized(vc){

                if((ordenThread == 5)&&(vc.turnofifthThread)){
                    //Contar
                    Contar(vc.i1Value, vc.i2Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i2Value){
                        prepararTerceraCuenta();
                        notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnofifthThread = false;
                        vc.turnofourthThread = true;
                        espaciarThreads();
                    }
                }


                if((ordenThread == 4)&&(vc.turnofourthThread)){
                    //Contar
                    Contar(vc.i1Value, vc.i2Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i2Value){
                        prepararTerceraCuenta();
                        notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnofourthThread = false;
                        vc.turnothirdThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 3)&&(vc.turnothirdThread)){
                    //Contar
                    Contar(vc.i1Value, vc.i2Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i2Value){
                        prepararTerceraCuenta();
                        notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnothirdThread = false;
                        vc.turnofifthThread = true;
                        espaciarThreads();
                    }
                }

                if(!vc.finSegundaCuenta){
                    vc.notify();
                    vc.wait();
                }

            }}catch(Exception e){}
        }




        //***********************************************
        //Bloque donde cuentan firstThread, secondThread.
        //***********************************************
        while(!vc.finTerceraCuenta){
            try{synchronized(vc){
                if((ordenThread == 1)&&(vc.turnofirstThread)){
                    //Contar
                    Contar(vc.i2Value, vc.i3Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i3Value){
                        prepararCuartaCuenta();
                        notifyAll();
                    }

                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnofirstThread = false;
                        vc.turnosecondThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 2)&&(vc.turnosecondThread)){
                    //Contar
                    Contar(vc.i2Value, vc.i3Value);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.i3Value){
                        prepararCuartaCuenta();
                        notifyAll();
                    }
                    //Sino: preparaciones para el siguiente thread
                    else{
                        vc.turnosecondThread = false;
                        vc.turnofirstThread = true;
                        espaciarThreads();
                    }
                }

                if(!vc.finTerceraCuenta){
                    vc.notify();
                    vc.wait();
                }

            }}catch(Exception e){}
        }

        //*******************************
        //Bloque donde cuenta fifthThread.
        //*******************************
        while(!vc.finCuartaCuenta){
            try{synchronized(vc){
                if((ordenThread == 5)&&(vc.turnofifthThread)){
                    //Contar
                    Contar(vc.i3Value, vc.MaxCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MaxCount){
                        prepararUltimaCuenta();
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread, que es el mismo
                    else{
                        espaciarThreads();
                    }
                }

                if(!vc.finCuartaCuenta){
                    vc.notify();
                    vc.wait();
                }

            }}catch(Exception e){}
        }

        //**************************************************************************************************
        //Bloque donde cuentan firstThread, secondThread, thirdThread, fourthThread, fifthThread. Para atras.
        //***************************************************************************************************
        while(!vc.finQuintaCuenta){
            try{synchronized(vc){
                if((ordenThread == 1)&&(vc.turnofirstThread)){
                    //Contar hacia atrás
                    ContarAtras(vc.MaxCount, vc.MinCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MinCount){
                        vc.finQuintaCuenta = true;
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread
                    else{
                        vc.turnofirstThread = false;
                        vc.turnosecondThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 2)&&(vc.turnosecondThread)){
                    //Contar hacia atrás
                    ContarAtras(vc.MaxCount, vc.MinCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MinCount){
                        vc.finQuintaCuenta = true;
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread
                    else{
                        vc.turnosecondThread= false;
                        vc.turnothirdThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 3)&&(vc.turnothirdThread)){
                    //Contar hacia atrás
                    ContarAtras(vc.MaxCount, vc.MinCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MinCount){
                        vc.finQuintaCuenta = true;
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread
                    else{
                        vc.turnothirdThread = false;
                        vc.turnofourthThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 4)&&(vc.turnofourthThread)){
                    //Contar hacia atrás
                    ContarAtras(vc.MaxCount, vc.MinCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MinCount){
                        vc.finQuintaCuenta = true;
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread
                    else{
                        vc.turnofourthThread = false;
                        vc.turnofifthThread = true;
                        espaciarThreads();
                    }
                }

                if((ordenThread == 5)&&(vc.turnofifthThread)){
                    //Contar hacia atrás
                    ContarAtras(vc.MaxCount, vc.MinCount);

                    //Si se llega al final: Preparaciones siguiente bloque
                    if(vc.contadorComun == vc.MinCount){
                        vc.finQuintaCuenta = true;
                        vc.notifyAll();
                    }
                    //Sino: Preparaciones para el siguiente thread
                    else{
                        vc.turnofifthThread = false;
                        vc.turnofirstThread = true;
                        espaciarThreads();
                    }
                }

                if(!vc.finQuintaCuenta){
                    vc.notify();
                    vc.wait();
                }

            }}catch(Exception e){}
        }
    }
}


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void mtdGo(View v){
            Thread mainThread = Thread.currentThread();

            EditText MinCount;
            MinCount = (EditText) findViewById(R.id.inputMinCount);
            int v_MinCount = Integer.parseInt(MinCount.getText().toString());

            EditText MaxCount;
            MaxCount = (EditText) findViewById(R.id.inputMaxCount);
            int v_MaxCount = Integer.parseInt(MaxCount.getText().toString());

            EditText Step;
            Step = (EditText) findViewById(R.id.inputStep);
            int v_Step = Integer.parseInt(Step.getText().toString());

            EditText StepsPerThread;
            StepsPerThread = (EditText) findViewById(R.id.inputStepsPerThread);
            int v_StepsPerThread = Integer.parseInt(StepsPerThread.getText().toString());

            EditText i1Value;
            i1Value = (EditText) findViewById(R.id.inputi1Value);
            int v_i1Value = Integer.parseInt(i1Value.getText().toString());

            EditText i2Value;
            i2Value = (EditText) findViewById(R.id.inputi2Value);
            int v_i2Value = Integer.parseInt(i2Value.getText().toString());

            EditText i3Value;
            i3Value = (EditText) findViewById(R.id.inputi3Value);
            int v_i3Value = Integer.parseInt(i3Value.getText().toString());

            //Añadir codigo para crear e inicializar
            //la varibale compartida oVC

            TipoNoPrimitivo oVC = new TipoNoPrimitivo(v_MinCount,v_MaxCount,v_Step,v_StepsPerThread,v_i1Value,v_i2Value,v_i3Value);

            myThread oThreadA = new myThread("Thread A", oVC);
            myThread oThreadB = new myThread("Thread B", oVC);
            myThread oThreadC = new myThread("Thread C", oVC);
            myThread oThreadD = new myThread("Thread D", oVC);
            myThread oThreadE = new myThread("Thread E", oVC);

            try {
                oThreadE.start();
                mainThread.sleep(100);
                oThreadD.start();
                mainThread.sleep(100);
                oThreadC.start();
                mainThread.sleep(100);
                oThreadB.start();
                mainThread.sleep(100);
                oThreadA.start();

                oThreadA.join();
                oThreadB.join();
                oThreadC.join();
                oThreadD.join();
                oThreadE.join();
            }
            catch (Exception e) {}

            Log.d("myTAG", "¡Los threads A, B, C y D han finalizado su ejecucion!");
    }
}