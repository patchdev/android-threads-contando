# Programación multi-thread en Android

El siguiente código es una muestra de una práctica en las que se crean 5 Thread, los cuales van contando en incrementos y decrementos desde un número mínimo a uno máximo, y vicerversa.

## Descripción

_Creación de una clase llamada MyThread que permita ejecutar threads capaces de contar de forma ascendente y descendente._

_Cada thread recibe su nombre dependiendo en la poscición que lleguen a run();_

_Primero: Threads 1, 2, 3, 4 cuentan desde MinCount a i1Value._

_Segundo: Threads 5, 4, 3 cuentan de i1Value a i2Value._

_Tercero: Threads 1 y 2 cuentasn de i2Value a i3Value._

_Cuarto: Thread 5 cuenta desde i3Value a MaxCount._

_Luego la cuenta es descendente._

_Quinto: Threads 1, 2, 3, 4 y 5 cuentan desde MaxCount a MinCount._

_El paso de conteo depende de los parámetros Steps y StepsPerThread._
